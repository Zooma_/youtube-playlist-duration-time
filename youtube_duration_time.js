// ==UserScript==
// @name             Youtube playlist duration
// @namespace        http://tampermonkey.net/
// @version          0.2
// @description      I count it for you
// @match            https://www.youtube.com/playlist?list=*
// @updateURL        https://gitlab.com/Zooma_/youtube-playlist-duration-time/-/raw/master/youtube_duration_time.js
// @license          MIT
// @grant           none
// @author        Zooma
// ==/UserScript==

// telegram:        @de_zooma
let res_minutes, res_hours, res_seconds;

let duration_time = document.createElement("span");
duration_time.id = "dur_time";
duration_time.setAttribute("style", "color:#f00;");

function addSpan() {
  let seconds = 0;
  let minutes = 0;
  let hours = 0;
  let timeStampList = [];

  if (document.querySelector("ul.pl-header-details")) {
    document.querySelector("ul.pl-header-details").append(duration_time);
    timeStampList = document.getElementsByClassName("timestamp");
  }
  else {
    document.querySelector("#stats").append(duration_time);
    timeStampList = document.querySelectorAll("span.style-scope.ytd-thumbnail-overlay-time-status-renderer");
  }

  for (let i = 0; i < timeStampList.length; i++) {
    let parsedTimestamp = timeStampList[i].textContent.trim().split(':');

    switch (parsedTimestamp.length) {
      case 2: // minutes, seconds
        minutes += +parsedTimestamp[0];
        seconds += +parsedTimestamp[1];
        break;
      case 3: // hours, minutes, seconds
        hours += +parsedTimestamp[0];
        minutes += +parsedTimestamp[1];
        seconds += +parsedTimestamp[2];
        break;
    }
  }
  res_minutes = Math.floor((hours * 60 + minutes + seconds / 60) % 60);
  res_hours = Math.floor(hours + minutes / 60 + seconds / 3600);
  res_seconds = (hours * 60 + minutes * 60 + seconds) % 60;

  let result = ` count videos: ${timeStampList.length} | ${res_hours} hours, ${res_minutes} minutes, ${res_seconds} seconds`;

  document.getElementById("dur_time").innerHTML = result;
}
let timer = setInterval(addSpan, 3000);
