// ==UserScript==
// @name         Youtube playlist duration
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  ah shit here we go again
// @match     	 https://www.youtube.com/playlist?list=*
// @grant    	 	 none
// @author 			 vk.com/zooma || t.me/de_zooma
// ==/UserScript==
let res_minutes, res_hours, res_seconds;

var duration_time = document.createElement("span");
duration_time.id = "dur_time";
duration_time.setAttribute("style", "color:#f00;");

function addSpan() {
  let seconds = 0;
  let minutes = 0;
  let hours = 0
  let list = [];

  if (document.querySelector("ul.pl-header-details")) {
    document.querySelector("ul.pl-header-details").append(duration_time);
    list = document.getElementsByClassName("timestamp");
  } else {
    document.querySelector("#stats").append(duration_time);
    list = document.querySelectorAll("span.style-scope.ytd-thumbnail-overlay-time-status-renderer");
  }

  for (let i = 0; i < list.length; i++) {
    switch (list[i].textContent.trim().split(":").length) {
      case 2:
        minutes += +list[i].textContent.trim().split(":")[0];
        seconds += +list[i].textContent.trim().split(":")[1];
        break;
      case 3:
        hours += +list[i].textContent.trim().split(":")[0];
        minutes += +list[i].textContent.trim().split(":")[1];
        seconds += +list[i].textContent.trim().split(":")[2];
        break;
    }
  }
  res_minutes = Math.floor((hours * 60 + minutes + seconds / 60) % 60);
  res_hours = Math.floor(hours + minutes / 60 + seconds / 3600);
  res_seconds = (hours * 60 + minutes * 60 + seconds) % 60;

  document.getElementById("dur_time").innerHTML = " count videos: " + list.length + " | " + res_hours + " hours, " + res_minutes + " minutes, " + res_seconds + " seconds";
}
//var timer = setInterval(addSpan, 3000);

function determineStyle() {
  let targetNode = "";
  if (document.getElementById("contents") != null) {
    targetNode = document.getElementById("contents");
    console.log("new style");
  } else {
    targetNode = document.getElementById("pl-video-table");
    console.log("old style");
  }
  return targetNode;
}

const mutation = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver;
const mutationConfig = { childList: true, subtree: true };

let onMutate = function(mutationsList) {
  mutationsList.forEach(mutation => {
    //console.log("works");
    addSpan();
  });
};

let observer = new mutation(onMutate);
observer.observe(determineStyle(), mutationConfig);